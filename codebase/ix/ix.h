#ifndef _ix_h_
#define _ix_h_

#include <vector>
#include <string>

#include "../rbf/rbfm.h"

# define IX_EOF (-1)  // end of the index scan

#define PRIMARY_FILE_NAME_SUFFIX "_primary"
#define OVERFLOW_FILE_NAME_SUFFIX "_overflow"

#define MAX_ENTRY_SIZE MAX_RECORD_SIZE
#define MAX_KEY_SIZE MAX_ENTRY_SIZE

#define ENTRY_SLOT_SIZE 16
#define NUM_OF_DIRECTORY_SLOTS (PAGE_SIZE / ENTRY_SLOT_SIZE / ENTRY_SLOT_SIZE - 1) /* =15 */
#define NUM_OF_META_SLOTS (NUM_OF_DIRECTORY_SLOTS + 1)  /* =16 */
#define NUM_OF_ENTRY_SLOTS (PAGE_SIZE / ENTRY_SLOT_SIZE - NUM_OF_META_SLOTS)  /* =240 */

#define REGULAR_ENTRY_SIZE (3*sizeof(int) + 2) /* =14 */

#define NEXT_OVERFLOW_PAGE_LOCATION (PAGE_SIZE - sizeof(int))
#define INSERT_SLOT_LOCATION (PAGE_SIZE - sizeof(int) - sizeof(unsigned char))
#define DELETED_FLAG_LOCATION (PAGE_SIZE - sizeof(int) - 2 * sizeof(unsigned char))
#define DIRECTORY_LOCATION (PAGE_SIZE - NUM_OF_META_SLOTS*ENTRY_SLOT_SIZE)
#define DIRECTORY_SIZE (NUM_OF_META_SLOTS-1)*ENTRY_SLOT_SIZE /* =240 */

#define SLOT_EMPTY 255
#define ENTRY_IS_ON_NEXT_PAGE 254

#define NEXT_PAGE_NOT_EXIST 0xffffffff

typedef enum 
{
  SUCCESS = 0,
  SCAN_EOF = -1,
  CREATE_FILE_ERROR = -10,
  DESTROY_FILE_ERROR = -11,
  OPEN_FILE_ERROR = -12,
  CLOSE_FILE_ERROR = -13,
  ENTRY_NOT_EXIST = -20
} IXErrorCode;

class IX_ScanIterator;
class IXFileHandle;


class IndexManager {
 public:
  static IndexManager* instance();

  // Create index file(s) to manage an index
  RC createFile(const string &fileName, const unsigned &numberOfPages);

  // Delete index file(s)
  RC destroyFile(const string &fileName);

  // Open an index and returns an IXFileHandle
  RC openFile(const string &fileName, IXFileHandle &ixfileHandle);

  // Close an IXFileHandle. 
  RC closeFile(IXFileHandle &ixfileHandle);


  // The following functions  are using the following format for the passed key value.
  //  1) data is a concatenation of values of the attributes
  //  2) For INT and REAL: use 4 bytes to store the value;
  //     For VarChar: use 4 bytes to store the length of characters, then store the actual characters.

  // Insert an entry to the given index that is indicated by the given IXFileHandle
  RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

  // Delete an entry from the given index that is indicated by the given IXFileHandle
  RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

  // scan() returns an iterator to allow the caller to go through the results
  // one by one in the range(lowKey, highKey).
  // For the format of "lowKey" and "highKey", please see insertEntry()
  // If lowKeyInclusive (or highKeyInclusive) is true, then lowKey (or highKey)
  // should be included in the scan
  // If lowKey is null, then the range is -infinity to highKey
  // If highKey is null, then the range is lowKey to +infinity
  
  // Initialize and IX_ScanIterator to supports a range search
  RC scan(IXFileHandle &ixfileHandle,
      const Attribute &attribute,
      const void        *lowKey,
      const void        *highKey,
      bool        lowKeyInclusive,
      bool        highKeyInclusive,
      IX_ScanIterator &ix_ScanIterator);

  // Generate and return the hash value (unsigned) for the given key
  unsigned hash(const Attribute &attribute, const void *key);
  
  
  // Print all index entries in a primary page including associated overflow pages
  // Format should be:
  // Number of total entries in the page (+ overflow pages) : ?? 
  // primary Page No.??
  // # of entries : ??
  // entries: [xx] [xx] [xx] [xx] [xx] [xx]
  // overflow Page No.?? liked to [primary | overflow] page No.??
  // # of entries : ??
  // entries: [xx] [xx] [xx] [xx] [xx]
  // where [xx] shows each entry.
  RC printIndexEntriesInAPage(IXFileHandle &ixfileHandle, const Attribute &attribute, const unsigned &primaryPageNumber);
  
  // Get the number of primary pages
  RC getNumberOfPrimaryPages(IXFileHandle &ixfileHandle, unsigned &numberOfPrimaryPages);

  // Get the number of all pages (primary + overflow)
  RC getNumberOfAllPages(IXFileHandle &ixfileHandle, unsigned &numberOfAllPages);
  
  RC getKeyFromEntry(const Attribute &attribute, const void *entryData, void *keyData);
  RC getRidFromEntry(const void *entryData, RID &rid);
  RC getNextFromEntry(const void *entryData, unsigned char &page, unsigned char &slot);

  RC getFirstEntry(vector<void *> &bucket, unsigned char hashValue, unsigned char &page, unsigned char &slot);
  RC getEntryFromSlot(const Attribute &attribute, vector<void *> &bucket, unsigned char page, unsigned char slot, void *entryData);
  
  int getKeyLength(const Attribute &attribute, const void *key);

  unsigned char hashInPage(const Attribute &attribute, const void *key);
  unsigned hashInFile(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key);
    
    
    static unsigned int intHash(int integer)
    {
        return (unsigned int)integer;
    }
    
    static unsigned int realHash(float fl)
    {
        float fl1 = fl * 100;
        return (unsigned int)fl1;
    }
    
    static unsigned int varCharHash(char *str)
    {
        unsigned int hash = 0;
        int i;
        for (i=0; *str; i++)
        {
            if ((i & 1) == 0)
            {
                hash ^= ((hash << 7) ^ (*str++) ^ (hash >> 3));
            }
            else
            {
                hash ^= (~((hash << 11) ^ (*str++) ^ (hash >> 5)));
            }
        }
        
        return (hash & 0x7FFFFFFF);
    }
  
 protected:
  IndexManager   ();                            // Constructor
  ~IndexManager  ();                            // Destructor

 private:
  static IndexManager *_index_manager;

  unsigned level;
  unsigned next;

  RC createPrimaryFile(const string &fileName, const unsigned &numberOfPages);
  RC createOverflowFile(const string &fileName, const unsigned &numberOfPages);

  RC destroyPrimaryFile(const string &fileName);
  RC destroyOverflowFile(const string &fileName);

  RC openPrimaryFile(const string &fileName);  
  RC openOverflowFile(const string &fileName);
    
  RC initializePrimaryFile(const string &fileName, const unsigned &numberOfPages);

  string getPrimaryFileName(const string &fileName);
  string getOverflowFileName(const string &fileName);
    
  unsigned int innerIntHash(int integer);
  unsigned int innerRealHash(float fl);
  unsigned int innerVarCharHash(char *str);
    
  // Generate and return the inner/outer hash value (unsigned) for the given key
  unsigned int doHash(const Attribute &attribute, const void *key, bool isInner);

  RC setNextToEntry(void *entryData, unsigned char page, unsigned char slot);
  RC addNextToEntry(const vector<void *> bucket, void *entryData, unsigned char pageHashValue);

  unsigned char getInsertSlot(const void *pageData);
  RC setInsertSlot(void *pageData, unsigned char insertSlot);

  unsigned char getSlotFromDirectory(const void *pageData, unsigned char hashValue);

  RC setFirstEntry(vector<void *> &bucket, unsigned char hashValue, unsigned char page, unsigned char slot);

  RC insertEntryToSlot(const Attribute &attribute, void *pageData, unsigned char slot, const void *entryData);
  RC deleteEntryFromSlot(const Attribute &attribute, void *pageData, unsigned char slot, const void *entryData);

  RC insertEntryInBucket(const Attribute &attribute, IXFileHandle &ixfileHandle, vector<void *> &bucket, unsigned char pageHashValue, void *entryData, bool &pageAdded, bool needReorganization);

  RC getFirstEntryLocationInFile(IXFileHandle &ixfileHandle, const Attribute &attribute, PageNum &primaryPageNum, unsigned char &currentInnerHash, unsigned char &page, unsigned char &slot);
  RC getFirstEntryLocationInBucket(vector<void *> &bucket, unsigned char &currentInnerHash, unsigned char &page, unsigned char &slot);

  RC findInsertLocation(IXFileHandle &ixfileHandle, const Attribute &attribute, vector<void *> &bucket, void *entryData, unsigned char &page, unsigned char &slot, bool &pageAdded, bool needReorganization);
  RC getFirstEntryInLink(vector<void *> &bucket, unsigned char pageHashValue, const void *key, const RID &rid, unsigned char &page, unsigned char &slot);

  RC generateEntry(const Attribute &attribute, const void *key, const RID &rid, unsigned char page, unsigned char slot, void *entryData);

  int getEntryLength(const Attribute &attribute, const void *entryData);
  int getEntryLength(const Attribute &attribute, const void *key, const RID &rid);

  RC split(IXFileHandle &ixfileHandle, const Attribute &attribute);
  RC prepareASize1BucketVector(IXFileHandle &ixfileHandle, vector<void *> &bucket);
  RC createBucketAndMoveNext(IXFileHandle &ixfileHandle, unsigned &next, unsigned &level, unsigned &currentFixedSize, vector<void *> &bucket);
    
  RC merge(IXFileHandle &ixfileHandle, const Attribute &attribute);
  RC combineTwoBucketVectors(IXFileHandle &ixfileHandle, vector<void *> bucket0, vector<void *> bucket1);
  RC prepareMetadata(IXFileHandle &ixfileHandle, unsigned &next, unsigned &level, unsigned &initialN, unsigned &currentFixedSize);
  RC reduceNext(IXFileHandle &ixfileHandle, unsigned &next, unsigned &level, unsigned &currentFixedSize);
    
  bool isSlotEmpty(void *aBucket);

  RC reorganizeBucket(IXFileHandle &ixfileHandle, const Attribute &attribute, vector<void *> &bucket);
  RC retrieveABucketEntries(vector<void *> bucket, const Attribute &attribute, vector<void *> &keys, vector<RID> &rids, vector<vector<int> *> &entriesAsLayered);
  RC getWholeLink(const Attribute &attribute, int index, vector<void *> &bucket, vector<void *> &keys, vector<RID> &rids, vector<vector<int> *> &entriesAsLayered);

  RC printEntry(const Attribute &attribute, void *key, RID rid);
  RC getBucketPageNumbers(IXFileHandle &ixfileHandle, vector<void *> &bucket, unsigned primaryPageNumber, vector<int> &bucketPageNumbers);

  RC freeEntriesAsLayeredVector(vector<vector<int> *> entriesAsLayered);
    
  RC setOnceDeletedFlag(vector<void *> bucket, bool flag);
  RC getOnceDeletedFlag(vector<void *> bucket, bool &flag);
};


class IX_ScanIterator {
 public:
  IX_ScanIterator();                // Constructor
  ~IX_ScanIterator();               // Destructor

  
  PageNum currentPrimaryPage;
  unsigned char currentInnerHash;
  unsigned char currentPage;
  unsigned char currentSlot;

  RC initializeIterator(const string &fileName, const Attribute &attribute, const void *lowKey, bool lowKeyInclusive, const void *highKey, bool highKeyInclusive, IndexManager &imInstance);


  RC getNextEntry(RID &rid, void *key);     // Get next matching entry
  RC close();                         // Terminate index scan

 private:
  IXFileHandle *ixfileHandle;
  IndexManager *im;
  Attribute attribute;

  bool isExactlyMatch;

  void *lowKey;
  void *highKey;

  bool lowKeyInclusive;
  bool highKeyInclusive;

  int lowKeyLength;
  int highKeyLength;

  vector<RID> retrivedRids;

 private:
  RC setIsExactlyMatch();

  RC rangeScan(RID &rid, void *key);
  RC hashingScan(RID &rid, void *key);

  bool hasScanned(RID &rid);

  bool validateNextEntry(vector<void *> bucket, void *entryData);
  bool validateSlot();
  bool isSatisfied(void *entryData);
  bool compKey(const void *keyData);
  bool compValue(const void *keyData, const void *compData, CompOp compOp);

  RC getNextEntryByHashing();
  RC getNextEntryByIteration();

  RC gotoNextEntry(IXFileHandle &ixfileHandle, vector<void *> &bucket, void *entryData);
};


class IXFileHandle {
public:
  // Put the current counter values of associated PF FileHandles into variables
    RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);

    IXFileHandle();               // Constructor
    ~IXFileHandle();              // Destructor

    RC bindFileDescriptors(int primaryFd, int overflowFd);
    RC getFileDescriptors(int &primaryFd, int &overflowFd);

    unsigned getNumberOfPrimaryPages();
    RC setNumberOfPrimaryPages(const unsigned numberOfPages);
    
    unsigned getNumberOfOverflowPages();
    RC setNumberOfOverflowPages(const unsigned numberOfPages);

    unsigned getLevelValue();
    RC setLevelValue(const unsigned level);

    unsigned getNextValue();
    RC setNextValue(const unsigned next);

    PageNum getNextOverflowPage(const void *pageData);
    RC setNextOverflowPage(void *pageData, PageNum pageNum);

    unsigned getInitialNumberOfPages();
    RC setInitialNumberOfPages(const unsigned numberOfPages);
    
    RC readEntriesCounter();
    RC writeEntriesCounter();
    
    RC processInitPrimaryFile(const unsigned &numberOfPages);
    RC initializePage(void * pageData);

    RC readPrimaryPage(PageNum pageNum, void *pageData);
    RC readOverflowPage(PageNum pageNum, void *pageData);
    
    RC writePrimaryPage(PageNum pageNum, const void *data);
    RC writeOverflowPage(PageNum pageNum, const void *data);

    RC appendPrimaryPage(const void *data);
    RC appendOverflowPage(const void *data);

    RC readBucket(PageNum primaryPageNum, vector<void *> &bucket);
    RC writeBucket(PageNum primaryPageNum, vector<void *> &bucket);
    RC appendPageToBucket(vector<void *> &bucket);
    RC freeBucket(vector<void *> &bucket);
    RC initializeBucket(vector<void *> &bucket);

    unsigned entriesCounter;

    unsigned numOfPrimaryPages;
    unsigned numOfOverflowPages;

    string fileName;

    
private:
    
    FileHandle primaryFileHandle;
    FileHandle overflowFileHandle;

    RC locateValue(const string &value);
    unsigned getValue();
    RC setValue(const unsigned value);
};

// print out the error message for a given return code
void IX_PrintError (RC rc);


#endif
