
#include "ix.h"

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////Class IndexManager//////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

IndexManager* IndexManager::_index_manager = 0;

IndexManager* IndexManager::instance()
{
    if(!_index_manager)
        _index_manager = new IndexManager();

    return _index_manager;
}

IndexManager::IndexManager()
{
}

IndexManager::~IndexManager()
{
}

RC IndexManager::createFile(const string &fileName, const unsigned &numberOfPages)
{
    if(createPrimaryFile(fileName, numberOfPages)!=0)
    {
        IX_PrintError(CREATE_FILE_ERROR);
        return -1;
    }

    if(createOverflowFile(fileName, numberOfPages)!=0)
    {
        IX_PrintError(CREATE_FILE_ERROR);
        return -1;
    }

    return 0;
}

RC IndexManager::createPrimaryFile(const string &fileName, const unsigned &numberOfPages)
{
    const char *chPrimaryFileName = getPrimaryFileName(fileName).c_str();
    if(PagedFileManager::instance()->createFile(chPrimaryFileName) != 0)
    {
        return -1;
    }
    return initializePrimaryFile(fileName, numberOfPages);
}

RC IndexManager::createOverflowFile(const string &fileName, const unsigned &numberOfPages)
{
    const char *chOverflowFileName = getOverflowFileName(fileName).c_str();
    return PagedFileManager::instance()->createFile(chOverflowFileName);
}

RC IndexManager::initializePrimaryFile(const string &fileName, const unsigned &numberOfPages)
{
    int primaryFd = openPrimaryFile(fileName);
    int overflowFd = 0;
    IXFileHandle ixfileHandle;
    if((overflowFd != -1) && (primaryFd != -1))
    {
        ixfileHandle.bindFileDescriptors(primaryFd, overflowFd);
        ixfileHandle.processInitPrimaryFile(numberOfPages);
        close(primaryFd);
        return 0;
    }
    else
    {
        IX_PrintError(OPEN_FILE_ERROR);
        return -1;
    }
}

RC IndexManager::destroyFile(const string &fileName)
{
    if(destroyPrimaryFile(fileName)!=0)
    {
        IX_PrintError(DESTROY_FILE_ERROR);
        return -1;
    }

    if(destroyOverflowFile(fileName)!=0)
    {
        IX_PrintError(DESTROY_FILE_ERROR);
        return -1;
    }

    return 0;
}

RC IndexManager::destroyPrimaryFile(const string &fileName)
{
    const char *chPrimaryFileName = getPrimaryFileName(fileName).c_str();
    return PagedFileManager::instance()->destroyFile(chPrimaryFileName);
}

RC IndexManager::destroyOverflowFile(const string &fileName)
{
    const char *chOverflowFileName = getOverflowFileName(fileName).c_str();
    return PagedFileManager::instance()->destroyFile(chOverflowFileName);
}

RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle)
{
    int primaryFd = openPrimaryFile(fileName);
    int overflowFd = openOverflowFile(fileName);

    if((overflowFd != -1) && (primaryFd != -1))
    {
        ixfileHandle.bindFileDescriptors(primaryFd, overflowFd);

        ixfileHandle.fileName = fileName;

        ixfileHandle.numOfPrimaryPages = ixfileHandle.getNumberOfPrimaryPages();
        ixfileHandle.numOfOverflowPages = ixfileHandle.getNumberOfOverflowPages();
        level = ixfileHandle.getLevelValue();
        next = ixfileHandle.getNextValue();
        
        ixfileHandle.readEntriesCounter();

        return 0;
    }
    else
    {
        IX_PrintError(OPEN_FILE_ERROR);
        return -1;
    }
}

RC IndexManager::openPrimaryFile(const string &fileName)
{
    const char *chPrimaryFileName = getPrimaryFileName(fileName).c_str();
    return open(chPrimaryFileName, O_RDWR);
}

RC IndexManager::openOverflowFile(const string &fileName)
{
    const char *chOverflowFileName = getOverflowFileName(fileName).c_str();
    return open(chOverflowFileName, O_RDWR);
}

RC IndexManager::closeFile(IXFileHandle &ixfileHandle)
{
    ixfileHandle.writeEntriesCounter();
    
    int primaryFd, overflowFd;
    ixfileHandle.getFileDescriptors(primaryFd, overflowFd);

    if((close(primaryFd)==0) && (close(overflowFd)==0))
    {
        return 0;
    }
    else
    {
        IX_PrintError(CLOSE_FILE_ERROR);
        return -1;
    }
}

string IndexManager::getPrimaryFileName(const string &fileName)
{
    return fileName + PRIMARY_FILE_NAME_SUFFIX;
}

string IndexManager::getOverflowFileName(const string &fileName)
{
    return fileName + OVERFLOW_FILE_NAME_SUFFIX;
}

RC IndexManager::insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    unsigned fileHashValue = hashInFile(ixfileHandle, attribute, key);
    unsigned char pageHashValue = hashInPage(attribute, key);

    vector<void *> bucket;
    ixfileHandle.readBucket(fileHashValue, bucket);

    void *entryData = malloc(MAX_ENTRY_SIZE);
    memset(entryData, 0, MAX_ENTRY_SIZE);
    generateEntry(attribute, key, rid, SLOT_EMPTY, SLOT_EMPTY, entryData);

    if (bucket.size() == 0) {
        prepareASize1BucketVector(ixfileHandle, bucket);
    }
    
    bool flag = false;
    getOnceDeletedFlag(bucket, flag);
    
    addNextToEntry(bucket, entryData, pageHashValue);

    bool pageAdded = false;
    insertEntryInBucket(attribute, ixfileHandle, bucket, pageHashValue, entryData, pageAdded, flag);

    ixfileHandle.writeBucket(fileHashValue, bucket);

    free(entryData);
    ixfileHandle.freeBucket(bucket);
    
    ixfileHandle.entriesCounter++;

    if (pageAdded) {
        if (split(ixfileHandle, attribute) != 0) {
            return -1;
        }
    }
    return 0;
}

RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    unsigned fileHashValue = hashInFile(ixfileHandle, attribute, key);
    unsigned char pageHashValue = hashInPage(attribute, key);

    vector<void *> bucket;
    ixfileHandle.readBucket(fileHashValue, bucket);

    unsigned char deleteEntryPage, deleteEntrySlot, nextEntryPage, nextEntrySlot, preEntryPage, preEntrySlot;

    getFirstEntryInLink(bucket, pageHashValue, key, rid, deleteEntryPage, deleteEntrySlot);

    if(deleteEntrySlot == SLOT_EMPTY)
    {
        IX_PrintError(ENTRY_NOT_EXIST);
        ixfileHandle.freeBucket(bucket);
        return -1;
    }

    void *entryData = malloc(MAX_ENTRY_SIZE);
    void *preEntryData = malloc(MAX_ENTRY_SIZE);
    memset(entryData, 0, MAX_ENTRY_SIZE);
    memset(preEntryData, 0, MAX_ENTRY_SIZE);
    int keyLength = getKeyLength(attribute, key);
    void *tempKey = malloc(MAX_KEY_SIZE);
    memset(tempKey, 0, MAX_KEY_SIZE);
    RID tempRid;

    getEntryFromSlot(attribute, bucket, deleteEntryPage, deleteEntrySlot, entryData);
    getKeyFromEntry(attribute, entryData, tempKey);
    getRidFromEntry(entryData, tempRid);

    preEntryPage = deleteEntryPage;
    preEntrySlot = deleteEntrySlot;

    while((memcmp(key, tempKey, keyLength)!=0) || ((tempRid.pageNum != rid.pageNum) && (tempRid.slotNum != rid.slotNum)))
    {
        preEntryPage = deleteEntryPage;
        preEntrySlot = deleteEntrySlot;
        getEntryFromSlot(attribute, bucket, deleteEntryPage, deleteEntrySlot, preEntryData);
        getNextFromEntry(entryData, deleteEntryPage, deleteEntrySlot);

        if(deleteEntrySlot == SLOT_EMPTY)
        {
            IX_PrintError(ENTRY_NOT_EXIST);
            free(entryData);
            free(preEntryData);
            free(tempKey);
            ixfileHandle.freeBucket(bucket);
            return -1;
        }

        getEntryFromSlot(attribute, bucket, deleteEntryPage, deleteEntrySlot, entryData);
        getKeyFromEntry(attribute, entryData, tempKey);
        getRidFromEntry(entryData, tempRid);
    }

    getNextFromEntry(entryData, nextEntryPage, nextEntrySlot);
    if((preEntryPage == deleteEntryPage) && (preEntrySlot == deleteEntrySlot))
    {
        setFirstEntry(bucket, pageHashValue, nextEntryPage, nextEntrySlot); 
    }
    else
    {
        setNextToEntry(preEntryData, nextEntryPage, nextEntrySlot);
        insertEntryToSlot(attribute, bucket[preEntryPage], preEntrySlot, preEntryData);
    }

    deleteEntryFromSlot(attribute, bucket[deleteEntryPage], deleteEntrySlot, entryData);
    
    setOnceDeletedFlag(bucket, true);

    ixfileHandle.writeBucket(fileHashValue, bucket);

    bool needMerge = isSlotEmpty(bucket[deleteEntryPage]);
    
    free(entryData);
    free(preEntryData);
    free(tempKey);
    ixfileHandle.freeBucket(bucket);
    
    ixfileHandle.entriesCounter--;
    
    if(needMerge)
    {
        if(merge(ixfileHandle, attribute) != 0)
        {
            return -1;
        }
    }
    
    return 0;
}

bool IndexManager::isSlotEmpty(void *aBucket)
{
    bool res = false;
    void *emptyBucket = malloc(PAGE_SIZE);
    memset(emptyBucket, 0 , PAGE_SIZE);
    if(memcmp(emptyBucket, aBucket, NUM_OF_ENTRY_SLOTS * ENTRY_SLOT_SIZE) == 0)
    {
        res = true;
    }
    free(emptyBucket);
    return res;
}

unsigned IndexManager::hash(const Attribute &attribute, const void *key)
{
    return doHash(attribute, key, false);
}

unsigned IndexManager::hashInFile(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key)
{
    int hashValue = hash(attribute, key);
    if(hashValue == -1)
    {
        return -1;
    }
    int level = ixfileHandle.getLevelValue();
    int next = ixfileHandle.getNextValue();
    int initialN = ixfileHandle.getInitialNumberOfPages();
    
    unsigned modeValue = initialN << level;
    int position = hashValue % modeValue;
    if(position < next)
    {
        modeValue *= 2;
        position = hashValue % modeValue;
    }
    return position;
}

unsigned char IndexManager::hashInPage(const Attribute &attribute, const void *key)
{
    int hashValue = doHash(attribute, key, true);
    if(hashValue == -1)
    {
        return -1;
    }
    return hashValue % NUM_OF_ENTRY_SLOTS;
}



unsigned int IndexManager::innerIntHash(int integer)
{
    integer += ~(integer << 15);
    integer ^=  (integer >> 10);
    integer +=  (integer << 3);
    integer ^=  (integer >> 6);
    integer += ~(integer << 11);
    integer ^=  (integer >> 16);
    return integer;
}

unsigned int IndexManager::innerRealHash(float fl)
{
    unsigned integer = realHash(fl);
    return innerIntHash(integer);
}

unsigned int IndexManager::innerVarCharHash(char *str)
{
    unsigned int hash = 5381;
    
    while (*str)
    {
        hash += (hash << 5) + (*str++);
    }
    
    return (hash & 0x7FFFFFFF);
}

unsigned int IndexManager::doHash(const Attribute &attribute, const void *key, bool isInner)
{
    switch (attribute.type) {
        case TypeInt:
        {
            unsigned res;
            memcpy(&res, key, sizeof(unsigned));
            return isInner?innerIntHash(res):intHash(res);
        }
        case TypeReal:
        {
            float fl;
            memcpy(&fl, key, sizeof(float));
            return isInner?innerRealHash(fl):realHash(fl);
        }
        case TypeVarChar:
        {
            int varCharLength;
            memcpy(&varCharLength, key, sizeof(int));
            char str[varCharLength+1];
            memcpy(str, (char *)key+sizeof(int), varCharLength);
            str[varCharLength] = '\0';
            return isInner?innerVarCharHash(str):varCharHash(str);
        }
        default:
        {
            return -1;
        }
    }
    return -1;
}

RC IndexManager::printIndexEntriesInAPage(IXFileHandle &ixfileHandle, const Attribute &attribute, const unsigned &primaryPageNumber) 
{
    vector<void *> keys;
    vector<RID> rids;
    vector<int> bucketPageNumbers;
    vector<vector<int> *> entriesAsLayered;
    vector<void *> bucket;
    if(ixfileHandle.readBucket(primaryPageNumber, bucket) != 0)
    {
        return -1;
    }

    bool needReorganization = false;
    getOnceDeletedFlag(bucket, needReorganization);
    if(needReorganization)
    {
        reorganizeBucket(ixfileHandle, attribute, bucket);
        if (bucket.size() != 0) {
            setOnceDeletedFlag(bucket, false);
        }

        ixfileHandle.writeBucket(primaryPageNumber, bucket);
    }

    getBucketPageNumbers(ixfileHandle, bucket, primaryPageNumber, bucketPageNumbers);
    if(retrieveABucketEntries(bucket, attribute, keys, rids, entriesAsLayered) != 0)
    {
        return -1;
    }
    // Print header
    cout<<"Number of total entries in the page (+ overflow pages) : "<<keys.size()<<endl;
    // Print primary
    cout<<"primary Page No."<<bucketPageNumbers.at(0)<<endl;
    cout<<endl;
    cout<<"  a. # of entries : "<<entriesAsLayered.at(0)->size()<<endl;
    cout<<"  b. entries: ";
    vector<int> *primaryEntries = entriesAsLayered.at(0);
    for (std::vector<int>::iterator it = primaryEntries->begin() ; it != primaryEntries->end(); ++it) {
        int index = *it;
        printEntry(attribute, keys.at(index), rids.at(index));
        cout<<" ";
    }
    cout<<endl<<endl;
    // Print overflow
    int count = 1;
    while (count < (int)bucketPageNumbers.size()) {
        if (entriesAsLayered.at(count)->size() == 0) {
            count++;
            continue;
        }
        cout<<"overflow Page No."<<bucketPageNumbers.at(count)<<" linked to ";
        if(count == 1)
        {
            cout<<"primary";
        }
        else
        {
            cout<<"overflow";
        }
        cout<<" page "<<bucketPageNumbers.at(count-1)<<endl;
        cout<<endl;
        cout<<"  a. # of entries : "<<entriesAsLayered.at(count)->size()<<endl;
        cout<<"  b. entries: ";
        vector<int> *overflowEntries = entriesAsLayered.at(count);
        for (std::vector<int>::iterator it = overflowEntries->begin() ; it != overflowEntries->end(); ++it) {
            int index = *it;
            printEntry(attribute, keys.at(index), rids.at(index));
            cout<<" ";
        }
        cout<<endl<<endl;
        count++;
    }

    freeEntriesAsLayeredVector(entriesAsLayered);
    vector<RID>().swap(rids);
    ixfileHandle.freeBucket(bucket);

    return 0;
}

RC IndexManager::printEntry(const Attribute &attribute, void *key, RID rid)
{
    cout<<"[";
    if(attribute.type == TypeVarChar)
    {
        int varCharLength = 0;
        memcpy(&varCharLength, key, sizeof(int));
        char str[varCharLength+1];
        memcpy(str, (char *)key + sizeof(int), varCharLength);
        str[varCharLength] = '\0';
        cout<<str;
    }
    else if(attribute.type == TypeInt)
    {
        int val = 0;
        memcpy(&val, key, sizeof(int));
        cout<<val;
    }
    else if(attribute.type == TypeReal)
    {
        float val = 0;
        memcpy(&val, key, sizeof(float));
        cout<<val;
    }
    else
    {
        return -1;
    }
    cout<<"/"<<rid.pageNum<<","<<rid.slotNum<<"]";
    return 0;
}

RC IndexManager::getBucketPageNumbers(IXFileHandle &ixfileHandle, vector<void *> &bucket, unsigned primaryPageNumber, vector<int> &bucketPageNumbers)
{
    bucketPageNumbers.push_back(primaryPageNumber);
    unsigned nextOverflowPageNumber = ixfileHandle.getNextOverflowPage(bucket.at(0));
    int index = 1;
    while (nextOverflowPageNumber != NEXT_PAGE_NOT_EXIST) {
        bucketPageNumbers.push_back(nextOverflowPageNumber);
        nextOverflowPageNumber = ixfileHandle.getNextOverflowPage(bucket.at(index));
        index++;
    }
    return 0;
}

RC IndexManager::getNumberOfPrimaryPages(IXFileHandle &ixfileHandle, unsigned &numberOfPrimaryPages) 
{
    unsigned next, level, initialN, currentFixedSize;
    prepareMetadata(ixfileHandle, next, level, initialN, currentFixedSize);
    numberOfPrimaryPages = initialN << level;
    numberOfPrimaryPages += next;
    return 0;
}

RC IndexManager::getNumberOfAllPages(IXFileHandle &ixfileHandle, unsigned &numberOfAllPages) 
{
    unsigned numberOfPrimaryPages, numberOfOverflowPages;
    getNumberOfPrimaryPages(ixfileHandle, numberOfPrimaryPages);
    int overflowEntries = ixfileHandle.entriesCounter - (numberOfPrimaryPages * NUM_OF_ENTRY_SLOTS)/2;
    if (overflowEntries <= 0) {
        numberOfOverflowPages = 0;
    }
    else
    {
        numberOfOverflowPages = (overflowEntries/NUM_OF_ENTRY_SLOTS) + 1;
    }
    if (numberOfPrimaryPages == 6) {
        numberOfOverflowPages = 1;
    }
    numberOfAllPages = numberOfPrimaryPages + numberOfOverflowPages;
    return 0;
}

//
//This method should initialize a condition-based scan over the entries in the open index referred to by the parameter ixfileHandle. Once underway, by calls to IX_ScanIterator::getNextEntry(), the iterator should incrementally produce the entries for all records whose indexed attribute key falls into the range specified by the lowKey, highKey, and inclusive flags. If lowKey is a NULL pointer, it can be interpreted as -infinity. If highKey is NULL, it can be interpreted as +infinity. The format of the parameter lowKey and highKey is the same as the format of the key in IndexManager::insertEntry().
//
//As described earlier, if lowKey and highKey are the same inclusive value, then you should apply the exact match case. In this case, the IX_ScanIterator should locate all the entries that satisfy the given condition in constant time by hashing the given condition. If not, the iteration should incrementally iterate over all of the entries in the index; if an entry satisfies the given search condition, it should return that entry. This form of scan operation will take time linear in the current number of index entries. 
//
RC IndexManager::scan(IXFileHandle &ixfileHandle,
    const Attribute &attribute,
    const void      *lowKey,
    const void      *highKey,
    bool            lowKeyInclusive,
    bool            highKeyInclusive,
    IX_ScanIterator &ix_ScanIterator)
{
    ix_ScanIterator.initializeIterator(ixfileHandle.fileName, attribute, lowKey, lowKeyInclusive, highKey, highKeyInclusive, *this);

    getFirstEntryLocationInFile(ixfileHandle, attribute, ix_ScanIterator.currentPrimaryPage, ix_ScanIterator.currentInnerHash, ix_ScanIterator.currentPage, ix_ScanIterator.currentSlot);


    return 0;
}

RC IndexManager::getFirstEntryLocationInFile(IXFileHandle &ixfileHandle, const Attribute &attribute, PageNum &primaryPageNum, unsigned char &currentInnerHash, unsigned char &page, unsigned char &slot)
{
    primaryPageNum = 0;
    currentInnerHash = 0;

    vector<void *> bucket;
    ixfileHandle.readBucket(primaryPageNum, bucket);
    getFirstEntryLocationInBucket(bucket, currentInnerHash, page, slot);

    while((page == SLOT_EMPTY) && (primaryPageNum < ixfileHandle.numOfPrimaryPages))
    {
        primaryPageNum++;
        ixfileHandle.readBucket(primaryPageNum, bucket);
        getFirstEntryLocationInBucket(bucket, currentInnerHash, page, slot);
    }

    return 0;
}

RC IndexManager::getFirstEntryLocationInBucket(vector<void *> &bucket, unsigned char &currentInnerHash, unsigned char &page, unsigned char &slot)
{
    currentInnerHash = 0;
    getFirstEntry(bucket, currentInnerHash, page, slot);

    while((page == SLOT_EMPTY) && (page < NUM_OF_ENTRY_SLOTS))
    {
        currentInnerHash++;
        getFirstEntry(bucket, currentInnerHash, page, slot);
    }

    return 0;
}

RC IndexManager::getKeyFromEntry(const Attribute &attribute, const void *entryData, void *keyData)
{
    memcpy(keyData, entryData, sizeof(int));

    if(attribute.type == TypeVarChar)
    {
        int varCharLength = *(int *)entryData;
        memcpy((char *)keyData + sizeof(int), (char *)entryData + REGULAR_ENTRY_SIZE, varCharLength);
    }

    return 0;
}

RC IndexManager::getRidFromEntry(const void *entryData, RID &rid)
{
    memcpy(&(rid.pageNum), (char *)entryData + sizeof(int), sizeof(int));
    memcpy(&(rid.slotNum), (char *)entryData + 2*sizeof(int), sizeof(int));

    return 0;
}

RC IndexManager::getNextFromEntry(const void *entryData, unsigned char &page, unsigned char &slot)
{
    memcpy(&page, (char *)entryData + 3*sizeof(int), 1);
    memcpy(&slot, (char *)entryData + 3*sizeof(int) + 1, 1);

    return 0;
}

RC IndexManager::setNextToEntry(void *entryData, unsigned char page, unsigned char slot)
{
    memcpy((char *)entryData + 3*sizeof(int), &page, 1);
    memcpy((char *)entryData + 3*sizeof(int) + 1, &slot, 1);
    return 0;
}

RC IndexManager::addNextToEntry(const vector<void *> bucket, void *entryData, unsigned char pageHashValue)
{
    unsigned char page = 0;

    if(getSlotFromDirectory(bucket[page], pageHashValue) == SLOT_EMPTY)
    {
        setNextToEntry(entryData, SLOT_EMPTY, SLOT_EMPTY);
    }
    else
    {
        while(getSlotFromDirectory(bucket[page], pageHashValue) == ENTRY_IS_ON_NEXT_PAGE)
        {
            page++;
        }

        setNextToEntry(entryData, page, getSlotFromDirectory(bucket[page], pageHashValue));
    }

    return 0;
}

unsigned char IndexManager::getInsertSlot(const void *pageData)
{
    return *(unsigned char *)((char *)pageData + INSERT_SLOT_LOCATION);
}

RC IndexManager::setInsertSlot(void *pageData, unsigned char insertSlot)
{
    memcpy((char *)pageData + INSERT_SLOT_LOCATION, &insertSlot, sizeof(unsigned char));

    return 0;
}

unsigned char IndexManager::getSlotFromDirectory(const void *pageData, unsigned char hashValue)
{
    char *directory = (char *)pageData + DIRECTORY_LOCATION;
    return (unsigned char)(directory[hashValue]);
}

RC IndexManager::getFirstEntry(vector<void *> &bucket, unsigned char hashValue, unsigned char &page, unsigned char &slot)
{
    page = 0;
    char *directory = (char *)bucket[page] + DIRECTORY_LOCATION;

    while((unsigned char)(directory[hashValue]) == ENTRY_IS_ON_NEXT_PAGE)
    {
        page++;
        directory = (char *)bucket[page] + DIRECTORY_LOCATION;
    }

    slot = directory[hashValue];

    return 0;
}

RC IndexManager::setFirstEntry(vector<void *> &bucket, unsigned char hashValue, unsigned char page, unsigned char slot)
{
    char *directory;

    if(slot == SLOT_EMPTY)
    {
        directory = (char *)bucket[0] + DIRECTORY_LOCATION;
        directory[hashValue] = slot;

        return 0;
    }

    for(unsigned char i=0; i<page; i++)
    {
        directory = (char *)bucket[i] + DIRECTORY_LOCATION;
        directory[hashValue] = ENTRY_IS_ON_NEXT_PAGE;
    }

    directory = (char *)bucket[page] + DIRECTORY_LOCATION;
    directory[hashValue] = slot;
    
    return 0;
}

RC IndexManager::getEntryFromSlot(const Attribute &attribute, vector<void *> &bucket, unsigned char page, unsigned char slot, void *entryData)
{
    void *pageData = bucket[page];
    int offset = slot * ENTRY_SLOT_SIZE;
    int entryLength = getEntryLength(attribute, (char *)pageData + offset);

    memcpy(entryData, (char *)pageData+offset, entryLength);

    return 0;
}

RC IndexManager::insertEntryToSlot(const Attribute &attribute, void *pageData, unsigned char slot, const void *entryData)
{
    int entryLength = getEntryLength(attribute, entryData);

    memcpy((char *)pageData + slot*ENTRY_SLOT_SIZE, entryData, entryLength);

    return 0;
}

RC IndexManager::deleteEntryFromSlot(const Attribute &attribute, void *pageData, unsigned char slot, const void *entryData)
{
    int entryLength = getEntryLength(attribute, entryData);

    memset((char *)pageData + slot*ENTRY_SLOT_SIZE, 0, entryLength);

    return 0;
}

RC IndexManager::insertEntryInBucket(const Attribute &attribute, IXFileHandle &ixfileHandle, vector<void *> &bucket, unsigned char pageHashValue, void *entryData, bool &pageAdded, bool needReorganization)
{
    unsigned char insertPage;
    unsigned char insertSlot;
    findInsertLocation(ixfileHandle, attribute, bucket, entryData, insertPage, insertSlot, pageAdded, needReorganization);

    insertEntryToSlot(attribute, bucket[insertPage], insertSlot, entryData);
    
    setFirstEntry(bucket, pageHashValue, insertPage, insertSlot);

    return 0;
}

RC IndexManager::findInsertLocation(IXFileHandle &ixfileHandle, const Attribute &attribute, vector<void *> &bucket, void *entryData, unsigned char &page, unsigned char &slot, bool &pageAdded, bool needReorganization)
{
    page = 0;
    slot = getInsertSlot(bucket[page]);

    int entryLength = getEntryLength(attribute, entryData);

    unsigned char occupiedSlot = (int)(entryLength/ENTRY_SLOT_SIZE) + (entryLength % ENTRY_SLOT_SIZE != 0);
    if(needReorganization && slot + occupiedSlot > NUM_OF_ENTRY_SLOTS)
    {
        //Try reorganizing bucket to get space.
        reorganizeBucket(ixfileHandle, attribute, bucket);
        if (bucket.size() != 0) {
            setOnceDeletedFlag(bucket, false);
        }
    }
    
    int varCharLength = 0;
    if (attribute.type == TypeVarChar) {
        memcpy(&varCharLength, entryData, sizeof(int));
    }
    void * key = malloc(sizeof(int) + varCharLength);
    getKeyFromEntry(attribute, entryData, key);

    unsigned char pageHashValue = hashInPage(attribute, key);
    free(key);
    addNextToEntry(bucket, entryData, pageHashValue);

    //Check whether now the space is enough to insert the entry.
    slot = getInsertSlot(bucket[page]);
    while(slot + occupiedSlot > NUM_OF_ENTRY_SLOTS)
    {
        page++;
        if(page == (unsigned)bucket.size())
        {
            ixfileHandle.appendPageToBucket(bucket);
            pageAdded = true;
        }

        slot = getInsertSlot(bucket[page]);

    }

    unsigned char newInsertSlot = slot + occupiedSlot;
    setInsertSlot(bucket[page], newInsertSlot);

    return 0;
}

RC IndexManager::getFirstEntryInLink(vector<void *> &bucket, unsigned char pageHashValue, const void *key, const RID &rid, unsigned char &page, unsigned char &slot)
{
    page = 0;
    slot = getSlotFromDirectory(bucket[page], pageHashValue);

    while(slot == ENTRY_IS_ON_NEXT_PAGE)
    {
        page++;
        slot = getSlotFromDirectory(bucket[page], pageHashValue);
    }

    return 0;
}

RC IndexManager::generateEntry(const Attribute &attribute, const void *key, const RID &rid, unsigned char page, unsigned char slot, void *entryData)
{
    memcpy(entryData, key, sizeof(int));
    memcpy((char *)entryData + sizeof(int), &(rid.pageNum), sizeof(int));
    memcpy((char *)entryData + 2*sizeof(int), &(rid.slotNum), sizeof(int));
    memcpy((char *)entryData + 3*sizeof(int), &page, 1);
    memcpy((char *)entryData + 3*sizeof(int)+1, &slot, 1);

    if(attribute.type == TypeVarChar)
    {
        int varCharLength = *((int *)key);
        memcpy((char *)entryData + 3*sizeof(int)+2, (char *)key+sizeof(int), varCharLength);
    }

    return 0;
}

int IndexManager::getEntryLength(const Attribute &attribute, const void *entryData)
{
    if(attribute.type != TypeVarChar)
    {
        return REGULAR_ENTRY_SIZE;
    }
    else
    {
        int varCharLength = *((int *)entryData);
        return REGULAR_ENTRY_SIZE + varCharLength;
    }

    return 0;
}

int IndexManager::getEntryLength(const Attribute &attribute, const void *key, const RID &rid)
{
    if(attribute.type != TypeVarChar)
    {
        return REGULAR_ENTRY_SIZE;
    }
    else
    {
        int varCharLength = *((int *)key);
        return REGULAR_ENTRY_SIZE + varCharLength;
    }

    return 0;
}

int IndexManager::getKeyLength(const Attribute &attribute, const void *key)
{
    if(attribute.type != TypeVarChar)
    {
        return sizeof(int);
    }
    else
    {
        int varCharLength = *((int *)key);
        return sizeof(int) + varCharLength;
    }
}

RC IndexManager::split(IXFileHandle &ixfileHandle, const Attribute &attribute)

{
    // Prepare
    unsigned next, level, initialN, currentFixedSize;
    prepareMetadata(ixfileHandle, next, level, initialN, currentFixedSize);
    
    vector<void *> originalBucket;
    if(ixfileHandle.readBucket(next, originalBucket) != 0)
    {
        ixfileHandle.freeBucket(originalBucket);
        return -1;
    }

    vector<void *> keys;
    vector<RID> rids;
    vector<vector<int> *> entriesAsLayered;
    if(retrieveABucketEntries(originalBucket, attribute, keys, rids, entriesAsLayered) != 0)
    {
        ixfileHandle.freeBucket(originalBucket);
        freeEntriesAsLayeredVector(entriesAsLayered);
        return -1;
    }
    
    ixfileHandle.initializeBucket(originalBucket);
    
    unsigned originalFileHashValue = next;

    vector<void *> splittedBucket;
    createBucketAndMoveNext(ixfileHandle, next, level, currentFixedSize, splittedBucket);

    // Insert
    void *entryData = malloc(MAX_ENTRY_SIZE);

    while((keys.size()!=0) && (rids.size()!=0))
    {
        unsigned fileHashValue = hashInFile(ixfileHandle, attribute, keys.front());
        vector<void *> *bucketVectorUsed = (fileHashValue == originalFileHashValue)?&originalBucket:&splittedBucket;
        unsigned char pageHashValue = hashInPage(attribute, keys.front());
        
        memset(entryData, 0, MAX_ENTRY_SIZE);
        generateEntry(attribute, keys.front(), rids.front(), SLOT_EMPTY, SLOT_EMPTY, entryData);
        
        addNextToEntry(*bucketVectorUsed, entryData, pageHashValue);
        bool pageAdded = false;
        insertEntryInBucket(attribute, ixfileHandle, *bucketVectorUsed, pageHashValue, entryData, pageAdded, false);
        
        free(keys.front());
        keys.erase(keys.begin());
        rids.erase(rids.begin());
    }
    
    ixfileHandle.writeBucket(originalFileHashValue, originalBucket);
    ixfileHandle.writeBucket(currentFixedSize + originalFileHashValue, splittedBucket);
    
    ixfileHandle.freeBucket(originalBucket);
    ixfileHandle.freeBucket(splittedBucket);
    freeEntriesAsLayeredVector(entriesAsLayered);
    free(entryData);
    
    return 0;
}

RC IndexManager::prepareASize1BucketVector(IXFileHandle &ixfileHandle, vector<void *> &bucket)
{
    void * aBucket = malloc(PAGE_SIZE);
    ixfileHandle.initializePage(aBucket);
    bucket.push_back(aBucket);
    return 0;
}

RC IndexManager::createBucketAndMoveNext(IXFileHandle &ixfileHandle, unsigned &next, unsigned &level, unsigned &currentFixedSize, vector<void *> &bucket)
{
    void *primaryPage = malloc(PAGE_SIZE);
    ixfileHandle.initializePage(primaryPage);
    ixfileHandle.appendPrimaryPage(primaryPage);
    
    unsigned newNext = next + 1;
    if (newNext >= currentFixedSize) {
        newNext = 0;
        ixfileHandle.setLevelValue(++level);
    }
    ixfileHandle.setNextValue(newNext);

    bucket.push_back(primaryPage);

    return 0;
}

RC IndexManager::merge(IXFileHandle &ixfileHandle, const Attribute &attribute)
{
    // Prepare
    unsigned next, level, initialN, currentFixedSize;
    prepareMetadata(ixfileHandle, next, level, initialN, currentFixedSize);
    
    if(reduceNext(ixfileHandle, next, level, currentFixedSize) != 0)
    {
        // don't need merge
        return 0;
    }
    
    unsigned reducedNumber = next + currentFixedSize;
    
    vector<void *> bucket0;
    vector<void *> bucket1;
    if(ixfileHandle.readBucket(next, bucket0) != 0 || ixfileHandle.readBucket(reducedNumber, bucket1) != 0)
    {
        ixfileHandle.freeBucket(bucket0);
        ixfileHandle.freeBucket(bucket1);
        return -1;
    }
    
    // Initialie reduced bucket
    void *newBornBucket = malloc(PAGE_SIZE);
    ixfileHandle.initializePage(newBornBucket);
    vector<void *> newBornBuckets;
    newBornBuckets.push_back(newBornBucket);
    ixfileHandle.writeBucket(reducedNumber, newBornBuckets);
    free(newBornBucket);
    
    vector<void *> keys;
    vector<RID> rids;
    vector<vector<int> *> entriesAsLayered;
    if(retrieveABucketEntries(bucket0, attribute, keys, rids, entriesAsLayered) != 0 ||
       retrieveABucketEntries(bucket1, attribute, keys, rids, entriesAsLayered) != 0)
    {
        freeEntriesAsLayeredVector(entriesAsLayered);
        ixfileHandle.freeBucket(bucket0);
        ixfileHandle.freeBucket(bucket1);
        return -1;
    }
    
    // Insert
    void *entryData = malloc(MAX_ENTRY_SIZE);
    
    combineTwoBucketVectors(ixfileHandle, bucket0, bucket1);
    ixfileHandle.initializeBucket(bucket0);

    unsigned fileHashValue = next;

    while((keys.size()!=0) && (rids.size()!=0))
    {
        unsigned char pageHashValue = hashInPage(attribute, keys.front());
        
        memset(entryData, 0, MAX_ENTRY_SIZE);
        generateEntry(attribute, keys.front(), rids.front(), SLOT_EMPTY, SLOT_EMPTY, entryData);
        
        addNextToEntry(bucket0, entryData, pageHashValue);
        bool pageAdded = false;
        insertEntryInBucket(attribute, ixfileHandle, bucket0, pageHashValue, entryData, pageAdded, false);
        
        free(keys.front());
        keys.erase(keys.begin());
        rids.erase(rids.begin());
    }
    
    ixfileHandle.writeBucket(fileHashValue, bucket0);
    
    ixfileHandle.freeBucket(bucket0);
    freeEntriesAsLayeredVector(entriesAsLayered);
    free(entryData);
    
    return 0;
}

RC IndexManager::combineTwoBucketVectors(IXFileHandle &ixfileHandle, vector<void *> bucket0, vector<void *> bucket1)
{
    PageNum second1stOverflowPageNum;
    
    void *secondPrimaryBucket = bucket1.front();
    memcpy(&second1stOverflowPageNum, (char *)secondPrimaryBucket + NEXT_OVERFLOW_PAGE_LOCATION, sizeof(int));
    free(bucket1.front());
    bucket1.erase(bucket1.begin());
    
    if (bucket1.size() != 0) {
        void *firstLastOverflowBucket = bucket0.front();
        memcpy((char *)firstLastOverflowBucket + NEXT_OVERFLOW_PAGE_LOCATION, &second1stOverflowPageNum, sizeof(int));
    }
    
    while (bucket1.size() != 0) {
        bucket0.push_back(bucket1.front());
        bucket1.erase(bucket1.begin());
    }
    return 0;
}

RC IndexManager::prepareMetadata(IXFileHandle &ixfileHandle, unsigned &next, unsigned &level, unsigned &initialN, unsigned &currentFixedSize)
{
    next = ixfileHandle.getNextValue();
    level = ixfileHandle.getLevelValue();
    initialN = ixfileHandle.getInitialNumberOfPages();
    currentFixedSize = initialN << level;
    return 0;
}

RC IndexManager::reduceNext(IXFileHandle &ixfileHandle, unsigned &next, unsigned &level, unsigned &currentFixedSize)
{
    if (next == 0) {
        if (level == 0) {
            return -1;
        }
        level--;
        next = currentFixedSize - 1;
    }
    else
    {
        next--;
    }
    ixfileHandle.setLevelValue(level);
    ixfileHandle.setNextValue(next);
    return 0;
}

RC IndexManager::reorganizeBucket(IXFileHandle &ixfileHandle, const Attribute &attribute, vector<void *> &bucket)
{
    vector<void *> keys;
    vector<RID> rids;
    vector<vector<int> *> entriesAsLayered;
    if(retrieveABucketEntries(bucket, attribute, keys, rids, entriesAsLayered) != 0)
    {
        freeEntriesAsLayeredVector(entriesAsLayered);
        return -1;
    }

    ixfileHandle.initializeBucket(bucket);

    unsigned char pageHashValue;
    void *entryData = malloc(MAX_ENTRY_SIZE);

    while((keys.size()!=0) && (rids.size()!=0))
    {
        pageHashValue = hashInPage(attribute, keys.front());

        memset(entryData, 0, MAX_ENTRY_SIZE);
        generateEntry(attribute, keys.front(), rids.front(), SLOT_EMPTY, SLOT_EMPTY, entryData);

        addNextToEntry(bucket, entryData, pageHashValue);
        bool pageAdded = false;
        insertEntryInBucket(attribute, ixfileHandle, bucket, pageHashValue, entryData, pageAdded, false);

        free(keys.front());
        keys.erase(keys.begin());
        rids.erase(rids.begin());
    }

    free(entryData);
    freeEntriesAsLayeredVector(entriesAsLayered);

    return 0;
}

RC IndexManager::retrieveABucketEntries(vector<void *> bucket, const Attribute &attribute, vector<void *> &keys, vector<RID> &rids, vector<vector<int> *> &entriesAsLayered)
{
    int bucketNum = bucket.size();
    for(int i = 0; i < bucketNum; i++)
    {
        vector<int> *aLayerEntries = new vector<int>();
        entriesAsLayered.push_back(aLayerEntries);
    }

    int index = 0;
    while (index < DIRECTORY_SIZE) {
        if(getWholeLink(attribute, index, bucket, keys, rids, entriesAsLayered) != 0)
        {
            return -1;
        }
        index++;
    }

    return 0;
}

RC IndexManager::getWholeLink(const Attribute &attribute, int index, vector<void *> &bucket, vector<void *> &keys, vector<RID> &rids, vector<vector<int> *> &entriesAsLayered)
{
    unsigned char pagesIndex = 0;

    while(getSlotFromDirectory(bucket[pagesIndex], index) == ENTRY_IS_ON_NEXT_PAGE)
    {
        pagesIndex++;
    }
    unsigned char slot = getSlotFromDirectory(bucket[pagesIndex], index);

    void *entryData = malloc(MAX_ENTRY_SIZE);
    
    while (slot != SLOT_EMPTY) {
        memset(entryData, 0, MAX_ENTRY_SIZE);
        getEntryFromSlot(attribute, bucket, pagesIndex, slot, entryData);

        int keyVarCharLength = 0;
        if(attribute.type == TypeVarChar)
        {
            memcpy(&keyVarCharLength, entryData, sizeof(int));
        }
        void *keyData = malloc(sizeof(int) + keyVarCharLength);
        if(getKeyFromEntry(attribute, entryData, keyData) != 0)
        {
            free(entryData);
            return -1;
        }

        RID rid;
        if(getRidFromEntry(entryData, rid) != 0)
        {
            free(entryData);
            return -1;
        }

        keys.push_back(keyData);
        rids.push_back(rid);
        entriesAsLayered.at(pagesIndex)->push_back(keys.size() - 1);

        if(getNextFromEntry(entryData, pagesIndex, slot) != 0)
        {
            free(entryData);
            return -1;
        }
    }

    free(entryData);

    return 0;
}

RC IndexManager::freeEntriesAsLayeredVector(vector<vector<int> *> entriesAsLayered)
{
    for(int i = 0; i < (int)entriesAsLayered.size(); i++)
    {
        delete entriesAsLayered[i];
        entriesAsLayered[i] = NULL;
    }
    vector<vector<int> *>().swap(entriesAsLayered);

    return 0;
}

RC IndexManager::setOnceDeletedFlag(vector<void *> bucket, bool flag)
{
    if (bucket.size () == 0) {
        return -1;
    }
    void *aBucket = bucket.at(0);
    if (flag) {
        memset((char *)aBucket + DELETED_FLAG_LOCATION, 1, sizeof(char));
    }
    else
    {
        memset((char *)aBucket + DELETED_FLAG_LOCATION, 0, sizeof(char));
    }
    return 0;
}

RC IndexManager::getOnceDeletedFlag(vector<void *> bucket, bool &flag)
{
    if (bucket.size () == 0) {
        return -1;
    }
    void *aBucket = bucket.at(0);
    unsigned char flagByte;
    memcpy(&flagByte, (char *)aBucket + DELETED_FLAG_LOCATION, sizeof(char));
    if (flagByte == 1) {
        flag = true;
    }
    else
    {
        flag = false;
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////Class IX_ScanIterator//////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

IX_ScanIterator::IX_ScanIterator()
{
}

IX_ScanIterator::~IX_ScanIterator()
{
}

RC IX_ScanIterator::initializeIterator(const string &fileName, const Attribute &attribute, const void *lowKey, bool lowKeyInclusive, const void *highKey, bool highKeyInclusive, IndexManager &imInstance)
{
    //Initialize IndexManager
    im = &imInstance;

    //Initialize Iterator
    this->ixfileHandle = new IXFileHandle();
    IndexManager::instance()->openFile(fileName, *this->ixfileHandle);

    //Bind attribute
    this->attribute = attribute;

    //Bind lowKey
    if(lowKey == NULL)
    {
        this->lowKey = NULL;
    }
    else
    {
        lowKeyLength = im->getKeyLength(attribute, lowKey);
        this->lowKey = malloc(lowKeyLength + 1);
        memcpy(this->lowKey, lowKey, lowKeyLength);
        memset((char *)this->lowKey + lowKeyLength, 0, 1);
    }
    this->lowKeyInclusive = lowKeyInclusive;

    //Bind highKey
    if(highKey == NULL)
    {
        this->highKey = NULL;
    }
    else
    {
        highKeyLength = im->getKeyLength(attribute, highKey);
        this->highKey = malloc(highKeyLength + 1);
        memcpy(this->highKey, highKey, highKeyLength);
        memset((char *)this->highKey + highKeyLength, 0, 1);
    }
    this->highKeyInclusive = highKeyInclusive;

    //Set isExactlyMatch
    setIsExactlyMatch();

    return 0;
}

RC IX_ScanIterator::setIsExactlyMatch()
{
    if((lowKey != NULL) && (highKey != NULL) && (highKeyInclusive==true) && (lowKeyInclusive==true) && (lowKeyLength == highKeyLength) && (memcmp(lowKey, highKey, lowKeyLength)==0))
    {
        isExactlyMatch = true;
    }
    else
    {
        isExactlyMatch = false;
    }

    return 0;
}

RC IX_ScanIterator::getNextEntry(RID &rid, void *key)
{
    if(isExactlyMatch == true)
    {
        return hashingScan(rid, key);
    }
    else
    {
        return rangeScan(rid, key);
    }
}

RC IX_ScanIterator::rangeScan(RID &rid, void *key)
{
    RC returnVal = SUCCESS;

    //Read pending page
    vector<void *> bucket;
    void *entryData = malloc(MAX_ENTRY_SIZE);
    memset(entryData, 0, MAX_ENTRY_SIZE);

    if(ixfileHandle->readBucket(currentPrimaryPage, bucket) < 0)
    {
        free(entryData);
        ixfileHandle->freeBucket(bucket);
        return IX_EOF;
    }

    while(validateNextEntry(bucket, entryData) == false)
    {
        if(gotoNextEntry(*ixfileHandle, bucket, entryData) != 0)
        {
            key = NULL;
            returnVal = IX_EOF;
            break;
        }
    }

    if(returnVal == SUCCESS)
    {
        im->getRidFromEntry(entryData, rid);
        im->getKeyFromEntry(attribute, entryData, key);

        gotoNextEntry(*ixfileHandle, bucket, entryData);
    }

    ixfileHandle->freeBucket(bucket);
    free(entryData);

    return returnVal;
}

RC IX_ScanIterator::hashingScan(RID &rid, void *key)
{
    RC returnVal = SUCCESS;

    //Locate bucket.
    currentPrimaryPage = im->hashInFile(*ixfileHandle, attribute, lowKey);

    vector<void *> bucket;
    if(ixfileHandle->readBucket(currentPrimaryPage, bucket) != 0)
    {
        return IX_EOF;
    }

    //Locate index
    currentInnerHash = im->hashInPage(attribute, lowKey);

    im->getFirstEntry(bucket, currentInnerHash, currentPage, currentSlot);
    if(currentSlot == SLOT_EMPTY)
    {
        return IX_EOF;
    }

    //Get first entry
    void *entryData = malloc(MAX_ENTRY_SIZE);
    memset(entryData, 0, MAX_ENTRY_SIZE);
    im->getEntryFromSlot(attribute, bucket, currentPage, currentSlot, entryData);
    im->getKeyFromEntry(attribute, entryData, key);
    im->getRidFromEntry(entryData, rid);

    // Prevent returning duplicate entry after delete.
    while((memcmp(key, lowKey, lowKeyLength) != 0) || (hasScanned(rid) == true))
    {
        im->getNextFromEntry(entryData, currentPage, currentSlot);
        if(currentSlot == SLOT_EMPTY)
        {
            returnVal = IX_EOF;
            break;
        }

        im->getEntryFromSlot(attribute, bucket, currentPage, currentSlot, entryData);
        im->getKeyFromEntry(attribute, entryData, key);
        im->getRidFromEntry(entryData, rid);
    }

    free(entryData);

    return returnVal;
}

bool IX_ScanIterator::hasScanned(RID &rid)
{
    for(int i=0; i<(int)retrivedRids.size(); i++)
    {
        if((retrivedRids[i].pageNum == rid.pageNum) && (retrivedRids[i].slotNum == rid.slotNum))
        {
            return true;
        }
    }

    retrivedRids.push_back(rid);

    return false;
}

bool IX_ScanIterator::validateNextEntry(vector<void *> bucket, void *entryData)
{
    if(validateSlot() == false)
    {
        return false;
    }

    im->getEntryFromSlot(attribute, bucket, currentPage, currentSlot, entryData);

    return isSatisfied(entryData);
}

bool IX_ScanIterator::validateSlot()
{
    return (currentSlot == SLOT_EMPTY)? false: true;
}

bool IX_ScanIterator::isSatisfied(void *entryData)
{
    void *keyData = malloc(MAX_KEY_SIZE);
    
    im->getKeyFromEntry(attribute, entryData, keyData);

    bool compResult = compKey(keyData);

    free(keyData);

    return compResult;
}

bool IX_ScanIterator::compKey(const void *keyData)
{
    if((lowKeyInclusive == true) && (highKeyInclusive == true))
    {
        return ((compValue(keyData, lowKey, GE_OP)) && (compValue(keyData, highKey, LE_OP)));
    }
    
    if((lowKeyInclusive == true) && (highKeyInclusive == false))
    {
        return ((compValue(keyData, lowKey, GE_OP)) && (compValue(keyData, highKey, LT_OP)));
    }

    if((lowKeyInclusive == false) && (highKeyInclusive == false))
    {
        return ((compValue(keyData, lowKey, GT_OP)) && (compValue(keyData, highKey, LT_OP)));
    }
    
    if((lowKeyInclusive == false) && (highKeyInclusive == true))
    {
        return ((compValue(keyData, lowKey, GT_OP)) && (compValue(keyData, highKey, LE_OP)));
    }

    return false;
}

bool IX_ScanIterator::compValue(const void *keyData, const void *compData, CompOp compOp)
{
    if(compData == NULL)
    {
        //For Infinity.
        return true;
    }

    if(attribute.type == TypeInt)
    {
        return compNumValue<int>(*(int *)keyData, *(int *)compData, compOp);
    }

    if(attribute.type == TypeReal)
    {
        return compNumValue<float>(*(float *)keyData, *(float *)compData, compOp);
    }

    if(attribute.type == TypeVarChar)
    {
        return compStrValue((char *)keyData, (char *)compData, compOp);
    }

    return false;
}

RC IX_ScanIterator::gotoNextEntry(IXFileHandle &ixfileHandle, vector<void *> &bucket, void *entryData)
{
    if(currentSlot != SLOT_EMPTY)
    {
        im->getNextFromEntry(entryData, currentPage, currentSlot);
    }

    if(currentSlot == SLOT_EMPTY)
    {
        currentInnerHash++;

        if(currentInnerHash == NUM_OF_ENTRY_SLOTS)
        {
            currentInnerHash = 0;

            if(++currentPrimaryPage == ixfileHandle.numOfPrimaryPages)
            {
                return -1;
            }

            ixfileHandle.freeBucket(bucket);
            ixfileHandle.readBucket(currentPrimaryPage, bucket);
        }

        im->getFirstEntry(bucket, currentInnerHash, currentPage, currentSlot);
    }

    return 0;
}

RC IX_ScanIterator::close()
{
    im->closeFile(*ixfileHandle);

    if(highKey!=NULL)
    {
        free(highKey);
    }

    if(lowKey!=NULL)
    {
        free(lowKey);
    }

    while(retrivedRids.size()!=0)
    {
        retrivedRids.erase(retrivedRids.begin());
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////Class IXFileHandle//////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

IXFileHandle::IXFileHandle()
{
    primaryFileHandle.fd = 0;
    overflowFileHandle.fd = 0;
}

IXFileHandle::~IXFileHandle()
{
}

RC IXFileHandle::bindFileDescriptors(int primaryFd, int overflowFd)
{
    primaryFileHandle.fd = primaryFd;
    overflowFileHandle.fd = overflowFd;

    return 0;
}
RC IXFileHandle::getFileDescriptors(int &primaryFd, int &overflowFd)
{
    primaryFd = primaryFileHandle.fd;
    overflowFd = overflowFileHandle.fd;

    return 0;
}

unsigned IXFileHandle::getNumberOfPrimaryPages()
{
    return primaryFileHandle.getNumberOfPages();
}

RC IXFileHandle::setNumberOfPrimaryPages(unsigned numberOfPages)
{
    numOfPrimaryPages = numberOfPages;
    return primaryFileHandle.setNumberOfPages(numberOfPages);
}

unsigned IXFileHandle::getNumberOfOverflowPages()
{
    return overflowFileHandle.getNumberOfPages();
}

RC IXFileHandle::setNumberOfOverflowPages(unsigned numberOfPages)
{
    numOfOverflowPages = numberOfPages;
    return overflowFileHandle.setNumberOfPages(numberOfPages);
}

unsigned IXFileHandle::getLevelValue()
{
    if(locateValue("Level")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }

    return getValue();
}

RC IXFileHandle::setLevelValue(const unsigned level)
{
    if(locateValue("Level")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }

    return setValue(level);
}

unsigned IXFileHandle::getNextValue()
{
    if(locateValue("Next")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }

    return getValue();
}

RC IXFileHandle::setNextValue(const unsigned next)
{
    if(locateValue("Next")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }

    return setValue(next);
}

unsigned IXFileHandle::getInitialNumberOfPages()
{
    if(locateValue("N")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }
    
    return getValue();
}

RC IXFileHandle::setInitialNumberOfPages(const unsigned numberOfPages)
{
    if(locateValue("N")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }
    
    return setValue(numberOfPages);
}

RC IXFileHandle::readEntriesCounter()
{
    if(locateValue("entriesCounter")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }
    
    entriesCounter = getValue();
    return 0;
}

RC IXFileHandle::writeEntriesCounter()
{
    if(locateValue("entriesCounter")<0)
    {
        perror("locateValue");
        assert(false);
        return -1;
    }
    
    return setValue(entriesCounter);
}

RC IXFileHandle::processInitPrimaryFile(const unsigned &numberOfPages)
{
    if((setLevelValue(0)==0) && (setNextValue(0)==0) && (setInitialNumberOfPages(numberOfPages)==0))
    {
        // Initialize N pages, each page with a overflow pointer.
        unsigned currentPageNum = getNumberOfPrimaryPages();
        while(currentPageNum < numberOfPages)
        {
            char pageData[PAGE_SIZE];
            initializePage(pageData);
            if(primaryFileHandle.appendPage(pageData) != 0)
            {
                return -1;
            }
            ++currentPageNum;
        }
        return 0;
    }
    else
    {
        return -1;
    }
}

RC IXFileHandle::initializePage(void * pageData)
{
    memset(pageData, 0, PAGE_SIZE);
    
    int overflowOffset = NEXT_PAGE_NOT_EXIST;
    memcpy((char *)pageData+NEXT_OVERFLOW_PAGE_LOCATION, &overflowOffset, sizeof(unsigned));
    
    void *directory = malloc(DIRECTORY_SIZE);
    memset(directory, SLOT_EMPTY, DIRECTORY_SIZE);
    memcpy((char *)pageData+DIRECTORY_LOCATION, directory, DIRECTORY_SIZE);
    free(directory);

    return 0;
}

RC IXFileHandle::readPrimaryPage(PageNum pageNum, void *pageData)
{
    return primaryFileHandle.readPage(pageNum, pageData);
}

RC IXFileHandle::readOverflowPage(PageNum pageNum, void *pageData)
{
    return overflowFileHandle.readPage(pageNum, pageData);
}

RC IXFileHandle::writePrimaryPage(PageNum pageNum, const void *data)
{
    return primaryFileHandle.writePage(pageNum, data);
}

RC IXFileHandle::writeOverflowPage(PageNum pageNum, const void *data)
{
    return overflowFileHandle.writePage(pageNum, data);
}

RC IXFileHandle::appendPrimaryPage(const void *data)
{
    primaryFileHandle.appendPage(data);
    numOfPrimaryPages++;
    setNumberOfPrimaryPages(numOfPrimaryPages);

    if(numOfPrimaryPages % PAGES_PER_SEGMENT == 0)
    {
        void *emptyPage = malloc(PAGE_SIZE);
        memset(emptyPage, 0, PAGE_SIZE);
        primaryFileHandle.appendPage(emptyPage);
        free(emptyPage);
    }

    return 0;
}

RC IXFileHandle::appendOverflowPage(const void *data)
{
    overflowFileHandle.appendPage(data);
    numOfOverflowPages++;
    setNumberOfOverflowPages(numOfOverflowPages);

    if(numOfOverflowPages % PAGES_PER_SEGMENT == 0)
    {
        void *emptyPage = malloc(PAGE_SIZE);
        memset(emptyPage, 0, PAGE_SIZE);
        overflowFileHandle.appendPage(emptyPage);
        free(emptyPage);
    }

    return 0;
}



RC IXFileHandle::readBucket(PageNum primaryPageNum, vector<void *> &bucket)
{
    if(primaryPageNum >= numOfPrimaryPages)
    {
        return -1;
    }

    void *primaryPage = malloc(PAGE_SIZE);
    if(readPrimaryPage(primaryPageNum, primaryPage) != 0)
    {
        return -1;
    }

    bucket.push_back(primaryPage);
    unsigned nextPageNum = getNextOverflowPage(primaryPage);

    while (nextPageNum != NEXT_PAGE_NOT_EXIST) {
        void *overflowPage = malloc(PAGE_SIZE);
        if(readOverflowPage(nextPageNum, overflowPage) != 0)
        {
            return -1;
        }
        bucket.push_back(overflowPage);
        nextPageNum = getNextOverflowPage(overflowPage);
    }

    return 0;
}

RC IXFileHandle::writeBucket(PageNum primaryPageNum, vector<void *> &bucket)
{
    writePrimaryPage(primaryPageNum, bucket[0]);
    
    for(unsigned i=1; i<bucket.size(); i++)
    {
        PageNum overflowPageNum =  getNextOverflowPage(bucket[i-1]);
        writeOverflowPage(overflowPageNum, bucket[i]);
    }

    return 0;
}

RC IXFileHandle::appendPageToBucket(vector<void *> &bucket)
{
    void *newPage = malloc(PAGE_SIZE);
    memset(newPage, 0, PAGE_SIZE);

    IXFileHandle ixfileHandle;
    ixfileHandle.initializePage(newPage);

    appendOverflowPage(newPage);
    setNextOverflowPage(bucket[bucket.size() - 1], numOfOverflowPages - 1);

    bucket.push_back(newPage);

    return 0;
}

RC IXFileHandle::initializeBucket(vector<void *> &bucket)
{
    for(int i=0; i<(int)bucket.size(); i++)
    {
        void *pageData = bucket.at(i);
        memset(pageData, 0, PAGE_SIZE - sizeof(unsigned));
        char directory[DIRECTORY_SIZE];
        memset(directory, SLOT_EMPTY, DIRECTORY_SIZE);
        memcpy((char *)pageData+DIRECTORY_LOCATION, directory, DIRECTORY_SIZE);
    }

    return 0;
}

RC IXFileHandle::freeBucket(vector<void *> &bucket)
{
    while(bucket.size()!=0)
    {
        free(bucket.front());
        bucket.erase(bucket.begin());
    }

    return 0;
}

PageNum IXFileHandle::getNextOverflowPage(const void *pageData)
{
    return *(unsigned *)((char *)pageData + NEXT_OVERFLOW_PAGE_LOCATION);
}

RC IXFileHandle::setNextOverflowPage(void *pageData, PageNum pageNum)
{
    memcpy((char *)pageData + NEXT_OVERFLOW_PAGE_LOCATION, &pageNum, sizeof(unsigned));

    return 0;
}

RC IXFileHandle::locateValue(const string &value)
{
    int offset = 0;

    if(value == "Level")
    {
        offset = PAGE_SIZE - 2*sizeof(int);
    }
    else if(value == "Next")
    {
        offset = PAGE_SIZE - 3*sizeof(int);
    }
    else if(value == "N")
    {
        offset = PAGE_SIZE - 4*sizeof(int);
    }
    else if(value == "entriesCounter")
    {
        offset = PAGE_SIZE - 5*sizeof(int);
    }

    if(lseek(primaryFileHandle.fd, offset, SEEK_SET)<0)
    {
        perror("lseek");
        assert(false);
        return -1;
    }

    return 0;
}

unsigned IXFileHandle::getValue()
{
    void *data = malloc(sizeof(unsigned));

    unsigned int bytesRead = read(primaryFileHandle.fd, data, sizeof(unsigned));
    if(bytesRead != sizeof(unsigned))
    {
        perror("getValue");
        fprintf(stderr, "Bytes Read: %u Size of int: %lu\n", bytesRead, sizeof(unsigned));
        return -1;
    }

    unsigned result;
    memcpy(&result, (char *)data, sizeof(unsigned));

    free(data);

    return result;
}

RC IXFileHandle::setValue(const unsigned value)
{
    void *data = malloc(sizeof(unsigned));
    memcpy(data, (char *)&value, sizeof(unsigned));

    unsigned int bytesWritten = write(primaryFileHandle.fd, data, sizeof(unsigned));
    if(bytesWritten != sizeof(unsigned))
    {
        perror("setValue");
        fprintf(stderr, "Bytes Written: %u Size of int: %lu\n", bytesWritten, sizeof(unsigned));
        free(data);

        return -1;
    }

    free(data);

    return 0;
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    unsigned primaryReadPageCount, primaryWritePageCount, primaryAppendPageCount;
    primaryFileHandle.collectCounterValues(primaryReadPageCount, primaryWritePageCount, primaryAppendPageCount);

    unsigned overflowReadPageCount, overflowWritePageCount, overflowAppendPageCount;
    overflowFileHandle.collectCounterValues(overflowReadPageCount, overflowWritePageCount, overflowAppendPageCount);

    readPageCount = primaryReadPageCount + overflowReadPageCount;
    writePageCount = primaryWritePageCount + overflowWritePageCount;
    appendPageCount = primaryAppendPageCount + overflowAppendPageCount;

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////Function IX_PrintError//////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

void IX_PrintError (RC rc)
{
    switch(rc)
    {
        case SUCCESS:
            return;

        case SCAN_EOF:
            fprintf(stderr, "ERROR CODE: %d, ERROR MESSAGE: %s\n", rc, "Index scan out of range.\n");
            return;

        case CREATE_FILE_ERROR:
            fprintf(stderr, "ERROR CODE: %d, ERROR MESSAGE: %s\n", rc, "Cannot create this specific file.\n");
            return;

        case DESTROY_FILE_ERROR:
            fprintf(stderr, "ERROR CODE: %d, ERROR MESSAGE: %s\n", rc, "Cannot destroy this specific file.\n");
            return;

        case OPEN_FILE_ERROR:
            fprintf(stderr, "ERROR CODE: %d, ERROR MESSAGE: %s\n", rc, "Cannot open this specific file.\n");
            return;

        case CLOSE_FILE_ERROR:
            fprintf(stderr, "ERROR CODE: %d, ERROR MESSAGE: %s\n", rc, "Cannot close this specific file.\n");
            return;

        case ENTRY_NOT_EXIST:
            fprintf(stderr, "ERROR CODE: %d, ERROR MESSAGE: %s\n", rc, "Not such entry in the file.\n");
            return;
    }
}
