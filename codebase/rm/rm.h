
#ifndef _rm_h_
#define _rm_h_

#include <string.h>
#include <vector>
#include <map>
#include <stdlib.h>

#include "../rbf/rbfm.h"

using namespace std;

#define TABLE_FILE_NAME "Tables"
#define COLUMN_FILE_NAME "Columns"
#define FILE_NAME_LEN 255
#define TABLE_NAME_LEN 255
#define COLUMN_NAME_LEN 255

#define MAX_TUPLE_SIZE MAX_RECORD_SIZE

# define RM_EOF (-1)  // end of a scan operator


// RM_ScanIterator is an iteratr to go through tuples
// The way to use it is like the following:
//  RM_ScanIterator rmScanIterator;
//  rm.open(..., rmScanIterator);
//  while (rmScanIterator(rid, data) != RM_EOF) {
//    process the data;
//  }
//  rmScanIterator.close();

class RM_ScanIterator {
public:
  RM_ScanIterator() {};
  ~RM_ScanIterator() {close();};

  // "data" follows the same format as RelationManager::insertTuple()
  RC getNextTuple(RID &rid, void *data);
  RC close();
  RC setRBFMSI(RBFM_ScanIterator* _rbfmsi);
    
private:
    RBFM_ScanIterator* _rbfmsi;
};


// Relation Manager
class RelationManager
{
public:
  static RelationManager* instance();

  RC createTable(const string &tableName, const vector<Attribute> &attrs);

  RC deleteTable(const string &tableName);

  RC getAttributes(const string &tableName, vector<Attribute> &attrs);

  RC insertTuple(const string &tableName, const void *data, RID &rid);

  RC deleteTuples(const string &tableName);

  RC deleteTuple(const string &tableName, const RID &rid);

  // Assume the rid does not change after update
  RC updateTuple(const string &tableName, const void *data, const RID &rid);

  RC readTuple(const string &tableName, const RID &rid, void *data);

  RC readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data);

  RC reorganizePage(const string &tableName, const unsigned pageNumber);

  // scan returns an iterator to allow the caller to go through the results one by one. 
  RC scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  // comparision type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attributeNames, // a list of projected attributes
      RM_ScanIterator &rm_ScanIterator);


// Extra credit
public:
  RC dropAttribute(const string &tableName, const string &attributeName);

  RC addAttribute(const string &tableName, const Attribute &attr);

  RC reorganizeTable(const string &tableName);



protected:
  RelationManager();
  ~RelationManager();

private:
    RC initTableMap();
    RC getTableDescription(vector<Attribute> &recordDescriptor);
    RC getAttributeDescription(vector<Attribute> &recordDescriptor);
    RC tableIdToRid(int tableId, RID &rid);
    RC getContentFileName(const string &tableName, string &fileName, bool isSystemTable);
    RC checkTableName(const string &tableName);
    int getSizeOfdata(vector<Attribute> &attr, void* data);
    RC getAttributesSelectively(const string &tableName, vector<Attribute> &attrs, bool needAll);
    RC doCreateTable(const string &tableName, const vector<Attribute> &attrs, bool fromOuter);
    
    std::map<string, int> _tableMap;
    std::map<int, vector<Attribute> > _columnsMap;
    RecordBasedFileManager* _rbfm;
  static RelationManager *_rm;
};

#endif
